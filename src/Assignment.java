import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Assignment {
    private JButton zaruSobaButton;
    private JButton shoyuRamenButton;
    private JButton zaruRamenButton;
    private JButton misoRamenButton;
    private JButton tanukiUdonButton;
    private JButton kituneUdonButton;
    private JTextPane receivedInfo;
    private JButton checkOutButton;
    private JPanel root;
    private JTextPane totalCosts;

    int total;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int cost){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            int confirmation2 = JOptionPane.showConfirmDialog(null,
                    "Would you use 10% off coupon?",
                    "Coupon Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if(confirmation2 == 0){
                JOptionPane.showMessageDialog(null, "It applied!\n" +
                        "Thank you for your ordering "+food+"! It will be served as soon as possible.");
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText + food + "  " + (int)(cost*0.9) + " yen" + "\n");

                total+=(int)(cost*0.9);
                totalCosts.setText("Total  "+total+" yen");
            }

            if(confirmation2 == 1){
                JOptionPane.showMessageDialog(null, "Thank you for your ordering "+food+"! " +
                        "It will be served as soon as possible.");
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText + food + "  " + cost + " yen" + "\n");

                total+=cost;
                totalCosts.setText("Total  "+total+" yen");
            }
        }
    }

    public Assignment() {
        zaruSobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodimage/zaruSoba.png")
        ));
        shoyuRamenButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodimage/shoyuRamen.png")
        ));
        zaruRamenButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodimage/zaruRamen.png")
        ));
        misoRamenButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodimage/misoRamen.png")
        ));
        tanukiUdonButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodimage/tanukiUdon.png")
        ));
        kituneUdonButton.setIcon(new ImageIcon(
                this.getClass().getResource("foodimage/kituneUdon.png")
        ));

        zaruSobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Zaru Ssoba",300);
            }
        });

        shoyuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shoyu Ramen",590);
            }
        });
        zaruRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Zaru Ramen",490);
            }
        });
        misoRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso Ramen",690);
            }
        });
        tanukiUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tanuki Udon",280);
            }
        });
        kituneUdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kitune Udon",390);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. " +
                            "The total price is " + total + " yen.");
                    totalCosts.setText(null);
                    receivedInfo.setText(null);
                    total = 0;
                    totalCosts.setText("Total  "+total+" yen");
                }
            }
        });
    }
}
